<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class create_head extends Model
{
    protected $table = 'head';
    protected $fillable = ['hair','face','eye','nose','lip'];
}
