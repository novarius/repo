<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class create extends Model
{
    protected $table = 'create';
    protected $fillabletable = ['head','body','arm','leg'];
}
