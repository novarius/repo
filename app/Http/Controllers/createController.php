<?php

namespace App\Http\Controllers;

use App\create;
use Illuminate\Http\Request;

class createController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\create  $create
     * @return \Illuminate\Http\Response
     */
    public function show($create)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\create  $create
     * @return \Illuminate\Http\Response
     */
    public function edit($create)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\create  $create
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $create)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\create  $create
     * @return \Illuminate\Http\Response
     */
    public function destroy($create)
    {
        //
    }
}
