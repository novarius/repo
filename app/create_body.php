<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class create_body extends Model
{
    protected $table = 'body';
    protected $fillable = ['shape','back'];
}
