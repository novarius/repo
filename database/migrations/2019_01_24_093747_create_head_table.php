<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('head', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('hair');
            $table->string('face');
            $table->string('eye');
            $table->string('nose');
            $table->string('lip');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('head');
    }
}
